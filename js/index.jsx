"use strict";
var HelloJSX_1 = require("./hellojsx/HelloJSX");
var HelloWorld_1 = require("./helloworld/HelloWorld");
var ReactDOM = require("react-dom");
var React = require("react");
console.log(React);
ReactDOM.render(<div>
    <HelloJSX_1.default foo="Hello"/>
    <HelloWorld_1.default foo="World"/>
</div>, document.getElementById("example"));
//# sourceMappingURL=index.jsx.map