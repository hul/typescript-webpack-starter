import HelloJSX from "./hellojsx/HelloJSX";
import HelloWorld from "./helloworld/HelloWorld";
import * as ReactDOM from "react-dom";
import * as React from "react";
console.log(React);
ReactDOM.render(<div>
    <HelloJSX foo="Hello" />
    <HelloWorld foo="World" />
</div>, document.getElementById("example"));
