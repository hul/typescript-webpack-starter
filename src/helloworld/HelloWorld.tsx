import * as React from "react";
import * as ReactDOM from "react-dom";

interface Props {
  foo: string;
}

export default class MyComponent extends React.Component<Props, {}> {
    render() {
        return <span>{this.props.foo}</span>
    }
}